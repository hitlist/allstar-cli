'use strict'

const {inspect} = require('util')
const {Command} = require('seeli')

module.exports = new Command({
  name: 'actions'
, description: 'List all system actions available'
, interactive: false
, flags: {}
, run: function(cmd, data) {
    const hemera = require('../lib/hemera')
    return new Promise((resolve, reject) => {
      let actions = []
      hemera.ready(() => {
        let id = null
        hemera.act({
          topic: 'stats'
        , cmd: 'registeredActions'
        , maxMessages$: -1
        }, (err, res) => {
          if (err) {
            clearTimeout(id)
            return reject(err)
          }
          actions = actions.concat(res.actions)
        })

        id = setTimeout(() => {
          const patterns = new Set()
          const filtered = actions.filter((item) => {
            const pattern = Object.values(item.pattern).join()
            if (patterns.has(pattern)) return false
            patterns.add(pattern)
            return true
          })
          resolve(inspect(filtered, {colors: true, depth: 20}))
        }, 1000)
      })
    })
  }
})
