'use strict'

module.exports = new Map([
  ['sys', require('./sys')]
, ['actions', require('./actions')]
])
