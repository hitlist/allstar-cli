'use strict'

const cli = require('seeli')

module.exports = new cli.Command({
  description: 'create a new user with a superuser grant'
, alias: 'createsuperuser'
, ui: 'dots10'
, usage: [
    `${cli.bold('Usage:')} allstar create superuser -u esatterwhite -F Eric -L Satterwhite`
  , `${cli.bold('Usage:')} allstar create superuser -u joeblow --name:first=Joe --name:last=Blow`
  ]
, flags: {
    username: {
      type: String
    , shorthand: 'u'
    , required: true
    , description: 'A unique username for the user'
    }
  , email: {
      type: String
    , shorthand: 'e'
    , requiredd: true
    , description: 'The email address for the user'
    }
  , 'name:first': {
      type: String
    , shorthand: 'F'
    , required: true
    , description: 'The user\'s first name'
    }
  , 'name:last': {
      type: String
    , shorthand: 'L'
    , required: true
    , description: 'The user\'s last name'
    }
  , password: {
      type: String
    , mask: true
    , shorthand: 'p'
    , required: false
    , description: 'Password'
    }
  , confirm: {
      type: String
    , mask: true
    , required: false
    , shorthand: 'pp'
    , description: 'Confirm password'
    }
  }
, run: function (cmd, data) {
    const ui = this.ui
    return new Promise((resolve, reject) => {
      ui.start(`creating user ${data.username}`)
      const hemera = require('../lib/hemera')
      hemera.ready(() => {
        hemera.act({
          topic: 'tools'
        , cmd: 'createsuperuser'
        , meta$: {
            user: {
              superuser: true
            }
          }
        , ...data
        }, (err, resp) => {
          hemera.close()
          hemera._nats.close(() => {})
          if (err) {
            ui.fail(err.message)
            return reject(err)
          }
          ui.succeed(resp)
          resolve(null)
        })
      })
    })
  }
})
