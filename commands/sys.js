'use strict'

const util = require('util')
const cli = require('seeli')
const Table = require('cli-table2')
const SYSTEM_ID = '00000000-0000-0000-0000-000000000000'

module.exports = new cli.Command({
  name: 'sys'
, description: 'Access to internal system commands'
, ui: 'dots12'
, usage: [
    'allstar sys user -a create --arg:username=esatterwhite --arg:name:first=eric --args:name:last=smith'
  , 'allstar sys role -a list --arg:filters:name__in=admin,coach --fields=name --fields=auth_role_id'
  ]
, flags: {
    action: {
      type: String
    , description: 'Desired action to perform'
    , shorthand: 'a'
    , required: true
    }
  , version: {
      type: String
    , description: 'Version of command to execute'
    , shorthand: 'v'
    , default: 'v1'
    }
  , arg: {
      type: [Object, Array]
    , description: 'things to send'
    }
  , fields: {
      type: [String, Array]
    , description: 'Field headers for tabulated results'
    , required: false
    }
  }
, run: function (cmd, data) {
    this.ui.start(`executing ${cmd}::${data.action} `)
    const conf = require('keef')
    return new Promise((resolve, reject) => {
      const hemera = require('../lib/hemera')
      hemera.ready(async () => {
        console.time(' query time')
        const result = await hemera.act({
          topic: cmd
        , cmd: data.action
        , version: data.version
        , meta$: {
            user: {id: SYSTEM_ID, superuser: true, content_type: 'system'}
          }
        , ...conf.get('arg')
        })
        console.timeEnd(' query time')

        if (!result) {
          this.ui.succeed(`${cmd} ${data.action} complete`)
          return resolve(null)
        }
        if (typeof result === 'string') resolve(result)
        if (!data.fields) return resolve(util.inspect(result.data || result, {depth: 20, colors: true, compact: true}))

        const table = new Table({
          head: data.fields
        })

        let items = result.data
        if (items.data && items.meta) {
          items = items.data
        }

        if (!Array.isArray(items)) items = [items]
        for (const item of items) {
          const row = []
          for (const field of data.fields) {
            row.push(getProperty(item, field))
          }
          table.push(row)
        }

        resolve(table.toString())
      })
    })
  }
})

function getProperty(obj, string, sep = '.') {
  const parts = string.split(sep)
  let ret = obj
  let last = parts.pop()
  let prop
  while (prop = parts.shift()) {
    ret = ret[prop]
    if (ret == null) return ret
  }
  return ret[last]
}

