#!/usr/bin/env node
'use strict'
const path = require('path')
const seeli = require('seeli')

process.env.PROJECT_ROOT = path.join(__dirname, '..')

seeli.set({
  'exitOnError': true
, 'color': 'red'
})
const commands = require('../commands')

for (const [name, command] of commands) {
  seeli.use(name, command)
}


if (require.main === module) seeli.run()
