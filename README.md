# allstar-cli

A command line tool for allstar suite of applications

## Installation

```bash
$ npm install -g @allstar/allstar-cli`
$ allstar --help
$ alstar sys --help
```

## configuration

global configuration for the tool can be added to a json file in:

* `/etc/@allstar/allstar-cli.json`
* `~/.config/@allstar/allstar-cli.json`

To work on a local cluster, configuration should look something like this:

```javascript
{
  "nats":{
    "user": null
  , "pass": null
  , "servers": [
      "nats://0.0.0.0:4222"
    , "nats://0.0.0.0:4223"
    , "nats://0.0.0.0:4224"
    ]
  }
}
```

