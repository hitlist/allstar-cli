'use strict'

module.exports = {
  "nats":{
    "user": null
  , "pass": null
  , "servers": [
      "nats://0.0.0.0:4222"
    , "nats://0.0.0.0:4223"
    , "nats://0.0.0.0:4224"
    ]
  }
}
