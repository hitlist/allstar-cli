'use strict'
/**
 * module to boot strap hemera
 * @module allstar-cli/lib/hemera
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires keef
 * @requires bitters
 * @requires nats
 * @requires nats-hemera
 * @requires @allstar/parse-hosts
 * @requires @allstar/hemera-acl
 **/
const conf = require('keef')
const nats = require('nats')
const Hemera = require('nats-hemera')
const hemeraJaeger = require('hemera-jaeger')
const stats = require('hemera-stats')
const joi = require('hemera-joi')
const parse = require('@allstar/parse-hosts')
const acl = require('@allstar/hemera-acl')

const nats_config = conf.get('nats')

const connection = {
  ...nats_config
, servers: parse(nats_config.servers)
}

const nc = nats.connect(connection)

const hemera = new Hemera(nc, {
  logLevel: 'silent'
, childLogger: true
, tag: 'cli'
})

hemera._nats = nc

hemera.use(hemeraJaeger, {
  serviceName: 'cli'
})

hemera.use(stats)
hemera.use(acl)
hemera.use(joi)
process.once('SIGTERM', onSignal)
process.once('SIGINT', onSignal)
module.exports = hemera

function onSignal() {
  hemera.close()
  nc.close(() => {})
  process.exit(0)
}
